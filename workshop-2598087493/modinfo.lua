name = "Show cooldown time"
description = [[This mod lets you see the cooldown time items in inventory. Additionally, it lets you use the Status Announcement mod to tell your friends whether your watch's ability is ready or not, and if it isn't, how much time it has left.
You can choose language in the options.]]
author = "surg"
version = "1.1.5"
icon_atlas = "modicon.xml"
icon = "modicon.tex"
api_version_dst = 10
dst_compatible = true
client_only_mod = true
configuration_options =
{
    {
        name = "language",
        label = "Language",
        hover = "Default language.",
        options =
        {
            {description = "Chinese", data = "chs", hover = "Chinese"},
            {description = "Deutsch", data = "de", hover = "Deutsch"},
            {description = "Dutch", data = "du", hover = "Dutch"},
            {description = "English", data = "en", hover = "English"},
            {description = "Portuguese", data = "pt", hover = "Portuguese"},
            {description = "Polish", data = "pl", hover = "Polish"},
            {description = "Spanish", data = "sp", hover = "Spanish"},
            {description = "Vietnamese", data = "vn", hover = "Vietnamese"},
            {description = "Русский", data = "ru", hover = "Russian"},
        },
        default = "en",
    },
}
