local _G = GLOBAL
local require = _G.require
local Text = require "widgets/text"

_G.TUNING.SHOWCOOLDOWNTIME = {}
_G.TUNING.SHOWCOOLDOWNTIME.LANGUAGE = GetModConfigData("language")

modimport("scripts/languages/sct_".._G.TUNING.SHOWCOOLDOWNTIME.LANGUAGE..".lua")

local function UpdateChargeTimer(self)
    local str = ""
    local minutesleft = 0
    local secondsleft = 0
    local timeleft = math.ceil(self.rechargetime - (self.rechargetime * self.rechargepct))

    if timeleft <= 0 then
        timeleft = 0
    end

    if timeleft < 60 then
        secondsleft = timeleft
    else
        minutesleft = math.floor(timeleft / 60)
        secondsleft = timeleft - (minutesleft * 60)
    end

    if timeleft > 0 then
        str = string.format("%02d:%02d", minutesleft, secondsleft)
    end

    self.recharge_timer:SetString(str)

    self.item.recharge_minutesleft = minutesleft
    self.item.recharge_secondsleft = secondsleft
    self.item.recharge_timeleft    = timeleft
end

AddClassPostConstruct("widgets/itemtile", function(self)
    if self.item:HasTag("rechargeable") then
        if not self.recharge_timer then
            self.recharge_timer = self:AddChild(Text(_G.NUMBERFONT, 42))

            if _G.JapaneseOnPS4() then
                self.recharge_timer:SetHorizontalSqueeze(0.7)
            end

            self.recharge_timer:SetPosition(5, -17, 0)
        end

        self.item.recharge_timeleft    = 0
        self.item.recharge_minutesleft = 0
        self.item.recharge_secondsleft = 0

        self.recharge_timer:SetString("")

        local original_SetChargePercent = self.SetChargePercent
        local original_SetChargeTime = self.SetChargeTime

        self.SetChargePercent = function(self, percent)
            original_SetChargePercent(self, percent)
            UpdateChargeTimer(self)
        end

        self.SetChargeTime = function(self, t)
            original_SetChargeTime(self, t)
            UpdateChargeTimer(self)
        end
    end
end)

-- compatibility "Status Announcer" mod
AddClassPostConstruct("screens/playerhud", function(self)
    self.inst:DoTaskInTime(0, function()
        if self._StatusAnnouncer ~= nil then
            local original_AnnounceItem = self._StatusAnnouncer.AnnounceItem
            
            self._StatusAnnouncer.AnnounceItem = function(statusannouncer, slot)
                local item = slot.tile.item

                if item:HasTag("rechargeable") then
                    local str = ""
                    local name = item:GetBasicDisplayName():lower()
                    local is_my = item.replica.inventoryitem:IsGrandOwner(self.owner) and true or false
                    local minutesleft = item.recharge_minutesleft
                    local secondsleft = item.recharge_secondsleft

                    if item.recharge_timeleft == 0 then
                        str = string.format(is_my and _G.STRINGS.SHOWCOOLDOWNTIME.MY_READY
                                                  or  _G.STRINGS.SHOWCOOLDOWNTIME.THIS_READY,
                                            name)
                    else
                        if minutesleft > 0 then
                            str = string.format(is_my and _G.STRINGS.SHOWCOOLDOWNTIME.MY_TIMELEFT
                                                      or  _G.STRINGS.SHOWCOOLDOWNTIME.THIS_TIMELEFT,
                                                name, minutesleft, secondsleft)
                        else
                            str = string.format(is_my and _G.STRINGS.SHOWCOOLDOWNTIME.MY_SECONDSLEFT
                                                      or  _G.STRINGS.SHOWCOOLDOWNTIME.THIS_SECONDSLEFT,
                                                name, secondsleft)
                        end
                    end

                    statusannouncer:Announce(str)
                else
                    original_AnnounceItem(statusannouncer, slot)
                end
            end
        end
    end)
end)
