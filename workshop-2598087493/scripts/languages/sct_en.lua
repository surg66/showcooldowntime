--English
GLOBAL.STRINGS.SHOWCOOLDOWNTIME =
{
    MY_READY = "My %s is ready to use.",
    MY_TIMELEFT = "My %s will be ready in %d minute %d seconds.",
    MY_SECONDSLEFT = "My %s will be ready in %d seconds.",
    THIS_READY = "This %s is ready to use.",
    THIS_TIMELEFT = "This %s will be ready in %d minute %d seconds.",
    THIS_SECONDSLEFT = "This %s will be ready in %d seconds."
}
