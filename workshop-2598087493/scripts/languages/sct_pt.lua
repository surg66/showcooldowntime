--Portuguese
GLOBAL.STRINGS.SHOWCOOLDOWNTIME =
{
    MY_READY = "Meu %s está pronto para ser utilizado.",
    MY_TIMELEFT = "Meu %s estará pronto em %d minutos e %d segundos.",
    MY_SECONDSLEFT = "Meu %s estará pronto em %d segundos.",
    THIS_READY = "Este %s está pronto para ser utilizado.",
    THIS_TIMELEFT = "Este %s estará pronto em %d minutos e %d segundos.",
    THIS_SECONDSLEFT = "Este %s estará pronto em %d segundos."
}
