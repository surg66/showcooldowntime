--Dutch
GLOBAL.STRINGS.SHOWCOOLDOWNTIME =
{
    MY_READY = "Mijn %s is klaar voor gebruik.",
    MY_TIMELEFT = "Mijn %s is bruikbaar in %d minuten en %d seconden.",
    MY_SECONDSLEFT = "Mijn %s is bruikbaar in %d seconden.",
    THIS_READY = "Deze %s is klaar voor gebruik.",
    THIS_TIMELEFT = "Deze %s is bruikbaar in %d minuten en %d seconden.",
    THIS_SECONDSLEFT = "Deze %s is bruikbaar in %d seconden."
}
