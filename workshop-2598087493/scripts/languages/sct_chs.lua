--Simplified Chinese
GLOBAL.STRINGS.SHOWCOOLDOWNTIME =
{
    MY_READY = "我的 %s 已就绪。",
    MY_TIMELEFT = "我的 %s 将在 %d 分 %d 秒后就绪。",
    MY_SECONDSLEFT = "我的 %s 将在 %d 秒后就绪。",
    THIS_READY = "这个 %s 已就绪.",
    THIS_TIMELEFT = "这个 %s 将在 %d 分 %d 秒后就绪。",
    THIS_SECONDSLEFT = "这个 %s 将在 %d 秒后就绪。"
}