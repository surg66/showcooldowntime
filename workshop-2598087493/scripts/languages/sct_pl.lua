--Polish
GLOBAL.STRINGS.SHOWCOOLDOWNTIME =
{
    MY_READY = "%s jest gotowy do użycia.",
    MY_TIMELEFT = "%s będzie gotowy za %d min %d sek.",
    MY_SECONDSLEFT = "%s będzie gotowy za %d sek.",
    THIS_READY = "%s jest gotowy do użycia.",
    THIS_TIMELEFT = "%s będzie gotowy za %d min %d sek.",
    THIS_SECONDSLEFT = "%s będzie gotowy za %d sek."
}
