--Deutsch
GLOBAL.STRINGS.SHOWCOOLDOWNTIME =
{
    MY_READY = "Mein %s ist bereit zur Nutzung.",
    MY_TIMELEFT = "Mein %s ist bereit in %d Minuten %d Sekunden.",
    MY_SECONDSLEFT = "Mein %s ist bereit in %d Sekunden.",
    THIS_READY = "Dieser %s ist bereit zur nutzung.",
    THIS_TIMELEFT = "Dieser %s ist bereit in %d Minuten %d Sekunden.",
    THIS_SECONDSLEFT = "Dieser %s ist bereit in %d Sekunden."
}
