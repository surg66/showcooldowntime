--Vietnamese
GLOBAL.STRINGS.SHOWCOOLDOWNTIME =
{
    MY_READY = "%s của tôi đang sẵn sàng để sử dụng.",
    MY_TIMELEFT = "%s của tôi sẽ sẵn sàng sau %d phút %d giây.",
    MY_SECONDSLEFT = "%s của tôi sẽ sẵn sàng sau %d giây.",
    THIS_READY = "%s này đang sẵn sàng để sử dụng.",
    THIS_TIMELEFT = "%s này sẽ sẵn sàng sau %d phút %d giây.",
    THIS_SECONDSLEFT = "%s này sẽ sẵn sàng sau %d giây. "
}
